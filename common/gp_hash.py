import hashlib
import array



def gp_data_length(gp) :
    n = 0
    for l in gp.data.layers :
        for f in l.frames : 
            for s in f.strokes : 
                n += len(s.points)
    return n


def pick_points(gp, skip) : 
    picked = []

    for l in gp.data.layers :
        for f in l.frames : 
            for s in f.strokes : 
                stroke_length = len(s.points)
                j = 0
                while j < stroke_length :
                    picked += list(s.points[j].co)
                    j += skip
    
    return picked


def get_animation_data(gp) :
    anim_data = []

    if gp.animation_data is not None and gp.animation_data.action is not None:
        for fcurve in gp.animation_data.action.fcurves:
            if fcurve.data_path in ["location", "rotation_euler", "scale"] :
                keyframes = [(kf.co.x, kf.co.y) for kf in fcurve.keyframe_points]
                anim_data.append(("%s[%d]" % (fcurve.data_path, fcurve.array_index), keyframes))
    
    anim_data.sort(key=lambda e : e[0])

    return anim_data


### PUBLIC ###


def hash(gp, pickrate=0.02, hex=False, tolerate_objects=True) :
    if type(gp).__name__ != "Object" :
        raise ValueError(f"Provided object is not a Blender object ({type(gp).__name__})")
    
    digest = lambda h : h.hexdigest() if hex else h.digest()
    h = hashlib.sha256()

    anim_data = str(get_animation_data(gp))
    h.update(anim_data.encode())

    if type(gp.data).__name__ != "GreasePencil" :
        if tolerate_objects :
            return digest(h)
        else :
            raise ValueError(f"Provided object doesn't have a legal type for hashing ({type(gp.data).__name__})")

    size = gp_data_length(gp)
    step = max(1,int(size*pickrate))
    points = pick_points(gp, step)

    points_hashable = array.array('f', points)
    h.update(points_hashable)

    visible_layers = [(l.info, l.opacity) for l in gp.data.layers if not l.hide]
    h.update(str(visible_layers).encode())

    return digest(h)