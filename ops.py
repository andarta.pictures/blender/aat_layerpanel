# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import hashlib
import struct


from .common.keymaps import register_keymap
from .common.utils import register_classes, unregister_classes
from .common.gp_utils import get_active_gp_object, get_scene_gp_objects
from .common.mode_manager import switch_mode
from .common import geometry_3D as geo3D, log_3D, blender_ui, timer, gp_hash

from . import core
from .core import gp_object_tools as gp_tools
from . import ui


class GPDRAW_OT_move_stroke(bpy.types.Operator):
    bl_idname = "anim.move_stroke"
    bl_label = "Move Stroke"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        print("M pressed")

        bpy.ops.gpencil.editmode_toggle()
        bpy.context.space_data.show_gizmo_object_translate = True

        return {'FINISHED'}


class GPEDIT_OT_return_to_draw(bpy.types.Operator):
    bl_idname = "anim.return_to_paint"
    bl_label = "Move Stroke"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        print("M released")

        bpy.ops.gpencil.paintmode_toggle()
        bpy.context.space_data.show_gizmo_object_translate = False

        return {'FINISHED'}


class GPEDIT_OT_add_GP_object(bpy.types.Operator):
    """Adds a new empty Grease Pencil object at origin."""
    bl_idname = "anim.gp_obj_add"
    bl_label = "Add Grease Pencil Object"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = gp_tools.create_new_gp_object("GP_NAME")
        obj.data.name = obj.name

        context.view_layer.objects.active = obj
        print("Active object : ", context.view_layer.objects.active)
        
        presets = {
            "DEFAULT" : ["GPLYR_STROKE", "GPLYR_FILL"],
            "MODELSHEET" : ["GPLYR_LINE_CLN", "GPLYR_STROKES", "GPLYR_COLO", "GPLYR_HINT", "GPLYR_TDW", "GPLYR_ROUGH", "GPLYR_FILL"],
            "ANIM" : ["GPLYR_LINE_CLN", "GPLYR_STROKES", "GPLYR_COLO", "GPLYR_HINT", "GPLYR_TDW", "GPLYR_ROUGH", "GPLYR_MASK", "GPLYR_FILL"],
            }
        
        preset_name = context.scene.layer_preset
        if preset_name not in presets.keys() :
            preset_name = "DEFAULT"

        for layer_name in reversed(presets[preset_name]) :
            gp_tools.create_new_gp_layer(obj, layer_name)

        return {'FINISHED'}

class GPEDIT_OT_add_GP_layer(bpy.types.Operator):
    """Adds a new empty Grease Pencil object at origin."""
    bl_idname = "anim.gp_layer_add"
    bl_label = "Add Grease Pencil Object"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = get_active_gp_object()
        gp_tools.create_new_gp_layer(obj, "GPLYR_NAME")

        return {'FINISHED'}

class GPEDIT_OT_remove_GP_object(bpy.types.Operator):
    """Removes selected Grease Pencil object."""
    bl_idname = "anim.gp_obj_remove"
    bl_label = "Remove Grease Pencil Object"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        object = get_active_gp_object()
        sorted_names = core.gp_tools.get_gp_object_display_order()

        if len(sorted_names) <= 1 :
            bpy.data.objects.remove(object, do_unlink=True)
            return {'FINISHED'}
        
        i = sorted_names.index(object.name)
        next_i = i-1 if i > 0 else 1
        next_obj_name = sorted_names[next_i]
        next_obj = bpy.data.objects[next_obj_name]

        bpy.data.objects.remove(object, do_unlink=True)
        bpy.context.view_layer.objects.active = next_obj
        core.event_manager.event("active_object_changed")

        return {'FINISHED'}


class GPEDIT_OT_test(bpy.types.Operator):
    """I AM A TEST. I DO NOT EXIST. PLEASE UNSEE ME. CLICK AT YOUR PERIL"""
    bl_idname = "anim.gp_obj_test"
    bl_label = "TEST"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        ui.display_elements.reset_collection()

        # collection = bpy.context.scene.gp_hierarchy_list
        # print("Pause status : ", context.scene.pause_layer_panel)

        # print(f"{len(context.scene.objects)} objects in scene.")

        # timer.print_totals(reset=True, display_percentage=False, precision=3, display_mean=True)

        return {'FINISHED'}
    
class GPEDIT_OT_move_to_collection_menu(bpy.types.Operator):
    """Moves Grease Pencil to collection."""
    bl_idname = "anim.gp_move_to_collection_menu"
    bl_label = "Move Grease Pencil to collection"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.wm.call_menu(name="OBJECT_MT_collection_menu")

        return {'FINISHED'}

class GPEDIT_OT_move_to_collection(bpy.types.Operator):
    """Moves Grease Pencil to collection."""
    bl_idname = "anim.gp_move_to_collection"
    bl_label = "Move Grease Pencil to collection"
    bl_options = {'REGISTER', 'UNDO'}

    move_to : bpy.props.StringProperty()

    def execute(self, context):
        
        scene = bpy.context.scene
        active_gp_index = scene.active_gp_index 
        display_elements = scene.gp_hierarchy_list

        moved_object_name = display_elements[active_gp_index].object_name

        if self.move_to == "Scene Collection" :
            move_to = bpy.context.scene.collection

        elif self.move_to == "Create new collection..." : 
            move_to = core.gp_tools.create_collection("Collection")            

        else :
            move_to = bpy.data.collections[self.move_to]

        selected_objects = bpy.context.selected_objects

        print("Moving %s to '%s'" % (str([o.name for o in selected_objects]), self.move_to))

        for o in selected_objects :
            core.gp_tools.change_collection(o, move_to)

        return {'FINISHED'}


class GPEDIT_OT_copy_selected_menu(bpy.types.Operator):
    """Copies selected grease pencil strokes to another location while preserving viewport size"""
    bl_idname = "anim.gp_copy_selected_menu"
    bl_label = "Copy selection"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        
        bpy.ops.wm.call_menu(name="OBJECT_MT_copy_selection_menu")

        return {'FINISHED'}

class GPEDIT_OT_copy_selected(bpy.types.Operator):
    """Copies selected grease pencil strokes to another location while preserving viewport size"""
    bl_idname = "anim.gp_copy_selected"
    bl_label = "Copy selection"
    bl_options = {'REGISTER', 'UNDO'}

    move_to : bpy.props.StringProperty()

    def execute(self, context):
        print("Moving to : ", self.move_to)
        dst_obj_name, dst_layer_name = self.move_to.split("/")
        dst_obj = bpy.data.objects[dst_obj_name]

        src_obj = context.view_layer.objects.active
        src_layer = src_obj.data.layers.active

        if dst_layer_name == "New layer" :
            dst_layer = dst_obj.data.layers.new("GP_Layer")
        else :
            dst_layer = dst_obj.data.layers[dst_layer_name]

        print("Moving selection from %s.%s to %s.%s"%(src_obj.name, src_layer.info, dst_obj.name, dst_layer.info))

        try :
            if context.scene.gp_copy_multiframe :
                core.gp_copy.multiframe_copy_selection_to_layer(src_obj, dst_obj, src_layer, dst_layer)
            else:
                core.gp_copy.copy_selection_to_layer(src_obj, dst_obj, src_layer, dst_layer, frame_cible=context.scene.frame_current)
            
        except AssertionError as e :
            blender_ui.show_message(e.args[0], "Error during copy", icon="ERROR")

        return {'FINISHED'}


class GPEDIT_OT_extra_tools_menu(bpy.types.Operator):
    """Extra tools for layer management."""
    bl_idname = "anim.gp_extra_tools_menu"
    bl_label = "Extra layer tools"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        
        bpy.ops.wm.call_menu(name="OBJECT_MT_extra_tools_menu")

        return {'FINISHED'}
    

class GPEDIT_OT_place_gp_origin(bpy.types.Operator):
    """Place origin point of Grease Pencil object."""
    bl_idname = "anim.gp_place_gp_origin"
    bl_label = "Place Grease Pencil origin..."
    bl_options = {'REGISTER', 'UNDO'}

    def modal(self, context, event) :
        if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
            mouse_position = (event.mouse_region_x, event.mouse_region_y)
            bpy.context.window.cursor_set("DEFAULT")

            world_pos = geo3D.region_to_location(mouse_position, self.obj.location, self.region, self.r3d)
            bpy.context.scene.cursor.location = world_pos

            previous_mode = switch_mode("OBJECT")
            bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
            previous_mode.switch()

            return {'FINISHED'}

        return {'RUNNING_MODAL'}

    def execute(self, context):

        obj = get_active_gp_object()
        self.obj = obj

        self.r3d, self.region = geo3D.get_region_settings()
        
        if obj is None :
            self.report({'ERROR'}, "/!\\ Place Grease Pencil origin : no grease pencil selected")
            return {'CANCELLED'}
        
        context.window_manager.modal_handler_add(self)
        bpy.context.window.cursor_set("CROSSHAIR")

        return {'RUNNING_MODAL'}
    

class GPEDIT_OT_activate_collection(bpy.types.Operator):
    """Activate collection."""
    bl_idname = "anim.activate_collection"
    bl_label = "Activate collection"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        collection = ui.hierarchy_analysis.get_collection(self.name)
        layer_collection = ui.hierarchy_analysis.get_layer_collection(collection)
        layer_collection.exclude = not layer_collection.exclude
        core.event_manager.force_display_update()

        return {'FINISHED'}
    

class GPEDIT_OT_fold_collection(bpy.types.Operator):
    """Fold collection."""
    bl_idname = "anim.fold_collection"
    bl_label = "Fold collection"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        
        if ui.display_elements.is_folded(self.name) :
            ui.display_elements.set_folded(self.name, False)
        
        else : 
            ui.display_elements.set_folded(self.name, True)
        
        # blender_ui.full_redraw()
        core.event_manager.force_state_update()

        return {'FINISHED'}
    

class GPEDIT_OT_layer_preset_menu_open(bpy.types.Operator):
    """Open layer preset menu"""
    bl_idname = "anim.layer_preset_menu_open"
    bl_label = "Open layer preset menu"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.wm.call_menu(name="OBJECT_MT_layer_preset_menu")
        return {'FINISHED'}
    

class GPEDIT_OT_layer_preset_change(bpy.types.Operator):
    """Change layer preset"""
    bl_idname = "anim.layer_preset_edit"
    bl_label = "Edit layer preset"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        bpy.context.scene.layer_preset = self.name
        print("New preset : ", bpy.context.scene.layer_preset)
        return {'FINISHED'}
    

class GPEDIT_OT_selection_preset_menu_open(bpy.types.Operator):
    """Open selection preset menu"""
    bl_idname = "anim.selection_preset_menu_open"
    bl_label = "Open selection preset menu"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.wm.call_menu(name="OBJECT_MT_selection_preset_menu")
        return {'FINISHED'}
    

class GPEDIT_OT_selection_preset_change(bpy.types.Operator):
    """Edit selection preset data"""
    bl_idname = "anim.selection_preset_edit"
    bl_label = "Edit selection preset"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        print("Switching to selection preset : ", self.name)
        presets = core.selection_preset.get_selection_preset_data()
        names = presets[self.name]
        print("Saved objects : ", names)

        if len(names) == 0 :
            return {'FINISHED'}

        bpy.context.view_layer.objects.active = bpy.data.objects[names[0]]

        for obj in bpy.context.view_layer.objects :
            obj.select_set(obj.name in names)

        print("Selected objects : ", [o.name for o in bpy.context.selected_objects])

        return {'FINISHED'}
    

class GPEDIT_OT_selection_preset_create(bpy.types.Operator):
    """Create new selection preset"""
    bl_idname = "anim.selection_preset_create"
    bl_label = "Create selection preset"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=150)
    
    def draw(self, context):
        layout = self.layout
        layout.prop(self, "name", text="Name : ")

    def execute(self, context):
        print("Creating new preset : ", self.name)
        core.selection_preset.add_new_preset(self.name)
        return {'FINISHED'}


class GPEDIT_OT_selection_preset_remove(bpy.types.Operator):
    """Remove selection preset"""
    bl_idname = "anim.selection_preset_remove"
    bl_label = "Remove selection preset"
    bl_options = {'REGISTER', 'UNDO'}

    name : bpy.props.StringProperty()

    def execute(self, context):
        core.selection_preset.remove_preset(self.name)
        return {'FINISHED'}

class GPEDIT_OT_extra_options_menu_open(bpy.types.Operator):
    """Open extra option menu"""
    bl_idname = "anim.extra_options_menu_open"
    bl_label = "Open extra options menu"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.wm.call_menu(name="OBJECT_MT_extra_options_menu")
        return {'FINISHED'}

class GPEDIT_OT_unpause_layer_panel(bpy.types.Operator):
    """Unpause layer panel"""
    bl_idname = "anim.unpause_layer_panel"
    bl_label = "Unpause layer panel"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        context.scene.pause_layer_panel = False
        return {'FINISHED'}


classes = [
    GPDRAW_OT_move_stroke, 
    GPEDIT_OT_return_to_draw,
    GPEDIT_OT_add_GP_object,
    GPEDIT_OT_add_GP_layer,
    GPEDIT_OT_remove_GP_object,
    GPEDIT_OT_test,
    GPEDIT_OT_move_to_collection_menu,
    GPEDIT_OT_move_to_collection,
    GPEDIT_OT_copy_selected_menu,
    GPEDIT_OT_copy_selected,
    GPEDIT_OT_extra_tools_menu,
    GPEDIT_OT_place_gp_origin,
    GPEDIT_OT_activate_collection,
    GPEDIT_OT_fold_collection,
    GPEDIT_OT_layer_preset_change,
    GPEDIT_OT_layer_preset_menu_open,
    GPEDIT_OT_selection_preset_menu_open,
    GPEDIT_OT_selection_preset_change,
    GPEDIT_OT_selection_preset_create,
    GPEDIT_OT_selection_preset_remove,
    GPEDIT_OT_extra_options_menu_open,
    GPEDIT_OT_unpause_layer_panel,
]

def register() :
    register_classes(classes)
    bpy.types.Scene.gp_copy_autoscale = bpy.props.BoolProperty(name="gp_copy_autoscale", default=True)
    bpy.types.Scene.gp_copy_keepgeo = bpy.props.BoolProperty(name="gp_copy_keepgeo", default=True)
    bpy.types.Scene.gp_copy_multiframe = bpy.props.BoolProperty(name="gp_copy_multiframe", default=False)
    bpy.types.Scene.gp_copy_selected_only = bpy.props.BoolProperty(name="gp_copy_selected_only", default=False)
    bpy.types.Scene.selection_preset_data = bpy.props.StringProperty(name="Selection Preset Data", default="")
    register_keymap(GPEDIT_OT_selection_preset_menu_open.bl_idname, "F6", value='PRESS', space_type="VIEW_3D")


def unregister() :
    del bpy.types.Scene.gp_copy_keepgeo
    del bpy.types.Scene.gp_copy_autoscale
    unregister_classes(classes)