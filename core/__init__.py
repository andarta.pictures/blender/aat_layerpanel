# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from typing import Optional


from ..common.gp_utils import get_active_gp_object
from ..common.mode_manager import set_gpencil_mode_safe

from . import gp_object_tools, gp_copy, event_manager, fake_opacity, selection_preset
from . import gp_object_tools as gp_tools

from .. import ui

# def test() :
#     gp_tools.create_collection("Yo")


def register() :
    event_manager.register()
    fake_opacity.register()

def unregister() :
    fake_opacity.unregister()
    event_manager.unregister()