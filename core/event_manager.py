# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from bpy.types import SpaceView3D
from time import time
import hashlib

from ..common import gp_utils, blender_ui, timer

from .. import core
from .. import ui

previous_hierarchy_state = None
previous_scene_name = ""

msgbus_owner = object()

current_blend_path = ""

event_lock = False
last_hierarchy_update_time = None
last_event_loop_time = None

fake_op_update_target = ""

force_state_update_flag = False


def force_display_update() :
    ui.display_elements.reset_collection()
    ui.display_elements.update_content()


def update_fake_opacity_system() :
    list_control = ui.display_elements.get_current_controller()
    active_item = list_control.get_active_item()

    if active_item is None :
        return
    
    if active_item.get_type() != "GP_OBJECT" :
        return
    
    fake_opacity_update_needed = core.fake_opacity.layer_opacity_changed(active_item.blender_object)
    if fake_opacity_update_needed :
        core.fake_opacity.update_all_layer_opacity(None, None)
    

def viewlayer_state_hash(viewlayer) :
    h = hashlib.sha256()
    vl_objects = [o for o in viewlayer.objects]
    infos = ""

    for o in vl_objects :
        infos += repr(o.location)
        infos += repr(o.rotation_euler)
        infos += repr(o.scale)
        infos += repr(o.hide_get())

    h.update(infos.encode())
    return h.hexdigest()

def display_elements_state_hash(display_elements) :
    h = hashlib.sha256()

    infos = ""
    for e in display_elements :
        infos += repr(e.name)
        infos += repr(e.type)
        infos += repr(e.path)
        infos += repr(e.h_order)
        infos += repr(e.selected)
        infos += repr(e.visible)
    
    infos += repr(ui.display_elements.get_folded_collections())

    h.update(infos.encode())
    return h.hexdigest()

def full_statehash() :
    t = timer.Timer("1. Statehash")
    viewlayer = bpy.context.view_layer
    display_elements = bpy.context.scene.gp_hierarchy_list

    statehash = viewlayer_state_hash(viewlayer)+display_elements_state_hash(display_elements)
    t.stop()
    return statehash

def force_state_update() :
    global force_state_update_flag
    force_state_update_flag = True

def state_changed(previous_state) :
    global force_state_update_flag
    
    if force_state_update_flag :
        force_state_update_flag = False
        return True, {'hash':full_statehash(),'time':time()}

    if previous_state is not None :
        elapsed_time = time() - previous_state['time']
        if elapsed_time < 0.1 :
            return False, previous_state
    
    statehash = full_statehash()
    state = {'hash':statehash,'time':time()}
    
    if previous_state is None :
        return True, state

    changed = statehash != previous_state['hash']
    
    return changed, state

def is_hierarchy_changed() :
    global previous_hierarchy_state, last_hierarchy_update_time
    
    if last_hierarchy_update_time is not None :
        elapsed_time = time() - last_hierarchy_update_time
        if elapsed_time < 1.0 :
            return False

    hierarchy = ui.hierarchy_analysis.get_hierarchy_list()

    if previous_hierarchy_state is None :
        change = True
    
    last_hierarchy_update_time = time()
    change = previous_hierarchy_state != str(hierarchy)

    if change :
        previous_hierarchy_state = str(hierarchy)

    return change

def is_scene_changed() :
    global previous_scene_name

    scene = bpy.context.scene

    if scene.name != previous_scene_name :
        previous_scene_name = scene.name
        return True
    
    else :
        return False


            

def force_fake_opacity_update(object) :
    global fake_op_update_target
    fake_op_update_target = object.name


def check_for_fake_opacity_manual_update() :
    global fake_op_update_target

    if fake_op_update_target != "" and fake_op_update_target in bpy.data.objects.keys() :
        obj = bpy.data.objects[fake_op_update_target]
        core.fake_opacity.update_fake_layer_opacity(obj)
        fake_op_update_target = ""


def is_file_loaded() :
    return bpy.context.blend_data.filepath != "" or bpy.data.is_dirty

def check_for_file_change() :
    global current_blend_path

    if bpy.context.blend_data.filepath == "" and not bpy.data.is_dirty :
        hierarchy_list = bpy.context.scene.gp_hierarchy_list
        for i in reversed(range(0, len(hierarchy_list))) :
            hierarchy_list.remove(i)

    elif current_blend_path != bpy.context.blend_data.filepath :
        print("Blend file change detected : ", bpy.context.blend_data.filepath)
        current_blend_path = bpy.context.blend_data.filepath
        ui.display_elements.reset_controller()
    

def event(*args) :
    global event_lock, last_event_loop_time

    if bpy.context.scene.pause_layer_panel or event_lock :
        return
    
    if last_event_loop_time is not None :
        elapsed_time = time() - last_event_loop_time
        if elapsed_time < 0.5 :
            return 

    last_event_loop_time = time()
    
    try :
        event_lock = True

        if not is_file_loaded() :
            return

        check_for_fake_opacity_manual_update()

        if is_hierarchy_changed() or is_scene_changed() :
            print(f"Resetting display elements : {(is_hierarchy_changed(), is_scene_changed())}")
            ui.display_elements.reset_collection()
            blender_ui.full_redraw()

        ui.display_elements.update_content()

    except (KeyError, AttributeError) as e :
        print("WARNING : error occured during layerpanel event loop")
        ui.display_elements.reset_collection()
        ui.display_elements.initialize_content()

    except :
        ui.display_elements.reset_collection()
        ui.display_elements.initialize_content()
        raise
    
    finally :
        event_lock = False


def on_GPO_VIEW_change(scene, context) :
    """Function handling any change to the GP_Object View."""
    # print("On view change")
    # ui.item_list.update_content()
    event("view_change")


def check_for_gp_layer_change() :
    gp_objects = gp_utils.get_all_gp_objects()

    for obj in gp_objects :
        if core.fake_opacity.layer_opacity_changed(obj) :
            return True
    
    return False
        

def outliner_update() :
    event("outliner_update")
    # ui.item_list.sync_selection()
    # blender_ui.full_redraw()


@bpy.app.handlers.persistent
def on_startup(arg) :
    print('Blender file loaded !')
    ui.display_elements.reset_collection()
    ui.display_elements.update_content()



@bpy.app.handlers.persistent
def msgbus_subscribe(*args):
    bpy.msgbus.subscribe_rna(
        key=(bpy.types.LayerObjects, "active"),
        owner=msgbus_owner,
        args=("active_object_changed",),
        notify=event,
    )


@bpy.app.handlers.persistent
def msgbus_unsubscribe(*args):
    bpy.msgbus.clear_by_owner(msgbus_owner)

def register() :

    msgbus_subscribe()

    bpy.app.handlers.load_post.append(msgbus_subscribe)
    bpy.app.handlers.load_pre.append(msgbus_unsubscribe)

    SpaceView3D.my_handler = SpaceView3D.draw_handler_add(outliner_update, (), 'WINDOW', 'POST_PIXEL')

    if not on_startup in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(on_startup)


def unregister() :

    msgbus_unsubscribe()
    bpy.app.handlers.load_post.remove(msgbus_subscribe)
    bpy.app.handlers.load_pre.remove(msgbus_unsubscribe)

    SpaceView3D.draw_handler_remove(SpaceView3D.my_handler, 'WINDOW')
    
    if on_startup in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(on_startup)