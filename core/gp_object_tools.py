# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np
from mathutils import Vector

from ..common import mode_manager, gp_utils
from . import event_manager

filter_mode = None
gp_object_ui_list = None

def get_geo(obj) :
    """Returns a list of points coordinates in the geometry of selected GP elements."""
    coords = []
    layers = obj.data.layers

    for l in layers :
        for f in l.frames:
            for s in f.strokes:
                for p in s.points:
                        coords.append(obj.matrix_world @ p.co)

    if len(coords) == 0 :
        coords.append(obj.matrix_world @ Vector((0,0,0)))

    return coords

def get_depth(obj) :
    """Returns the Y depth of the target object, from its center of mass.
    - obj : Blender object"""

    if not isinstance(obj.data, bpy.types.GreasePencil) :
        return 0.0
    
    geo = np.array(get_geo(obj))

    centroid = np.mean(geo, axis=0)

    return centroid[1]

def get_collection_tree() :
    parent_child = []

    for parent_collection in bpy.data.collections :
        for child in parent_collection.children.keys() :
            parent_child.append((parent_collection.name, child))
    
    return parent_child

def get_parent_collection(name, relations) :
    for parent, child in relations :
        if child == name :
            return parent
    return None

def get_collection_path(obj) :

    if type(obj) is bpy.types.GreasePencil :
        
        if len(obj.users_collection) == 0 :
            return []
        
        collection = obj.users_collection[0].name

    elif type(obj) is bpy.types.Collection :
        obj.name

    else :
        return []

    relations = get_collection_tree()
    path = [collection]
    
    for i in range(0,100) :
        parent = get_parent_collection(collection, relations)
        if parent is None :
            break
        collection = parent
        path = [parent] + path
    
    return path


def get_collection_by_name(name) :
    """Returns a Blender Collection object.
    - name : name of the collection"""

    if name == "Scene Collection" :
        return bpy.context.scene.collection

    for c in bpy.data.collections :
        if c.name == name :
            return c

# def get_folded_collection_list() :
#     """Returns the list of collection names that have been folded (list of String)."""

#     scene = bpy.context.scene
#     if len(scene.folded_collections) > 0 :
#         return scene.folded_collections.split(", ")
#     else :
#         return []


def collection_contains_objects(element) :
    
    if len(element.collection_content) == 0 :
        return False
    
    collection_names = bpy.data.collections.keys()
    
    children = element.collection_content.split(", ")
    if all([c in collection_names for c in children]) :
        return False
    
    return True

def create_collection(name) :
    if name in bpy.data.collections.keys() :
        for i in range(1,500) :
            new_name = "%s.%03d"%(name, i)
            if new_name not in bpy.data.collections.keys() :
                name = new_name
                break
    
    collection = bpy.data.collections.new(name=name)
    bpy.context.scene.collection.children.link(collection)

    return collection

# def create_layer(gp_object, name="GP_Layer") :
#     layers = gp_object.data.layers
#     if name in layers.keys() :
#         for i in range(1,200) :
#             new_name = "%s.%03d"

    # new(name, set_active=True)


def load_element(element_name) :
    if element_name in bpy.data.objects.keys() :
        return bpy.data.objects[element_name]
    
    elif element_name in bpy.data.collections.keys() :
        return bpy.data.collections[element_name]


def change_collection(element, collection) :

    scene = bpy.context.scene

    if isinstance(element, bpy.types.Object) :
        for c in element.users_collection :
            c.objects.unlink(element)

        collection.objects.link(element)
    
    elif isinstance(element, bpy.types.Collection) :
        all_collections = [c for c in bpy.data.collections] + [scene.collection]
        
        for c in all_collections :
            if element.name in c.children.keys() :
                print("Unlinking '%s' from '%s'" % (element.name, c.name))
                c.children.unlink(element)

        collection.children.link(element)

def create_new_gp_object(name) :
    scene = bpy.context.scene
    gp_data = bpy.data.grease_pencils.new("Bright Pencil")
    gp_ob = bpy.data.objects.new(name, gp_data)
    
    collection = scene.collection
    active_gp = gp_utils.get_active_gp_object()
    if active_gp is not None :
        collection = active_gp.users_collection[0]
    
    collection.objects.link(gp_ob)

    previous_mode = mode_manager.switch_mode("OBJECT")
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = gp_ob
    gp_ob.select_set(True)
    gp_ob.use_grease_pencil_lights = False

    event_manager.event("active_object_changed")

    previous_mode.switch()

    return gp_ob

def create_new_gp_layer(gp_object, name) :
    layer = gp_object.data.layers.new(name)
    layer.use_lights = False
    f = layer.frames.new(1)
    f.clear()


def set_gp_object_filter_list(ui_list) :
    global gp_object_ui_list
    gp_object_ui_list = ui_list

def get_filter_order() :
    global gp_object_ui_list
    filter_mode = "use_order_hierarchy" if gp_object_ui_list.use_order_hierarchy else "use_order_depth"
    return filter_mode

def get_gp_object_display_order() :
    scene = bpy.context.scene

    gp_object_names = []
    gp_objects_sort = []

    order_mode = get_filter_order()

    for display_element in scene.gp_hierarchy_list :
        if display_element.type == "GP_OBJECT" :

            obj_name = display_element.name
            obj = bpy.data.objects[obj_name]
            gp_object_names.append(obj_name)

            if order_mode == "use_order_depth" :
                gp_objects_sort.append(obj.location.y)
            
            else :
                gp_objects_sort.append(display_element.h_order)
    
    order = np.argsort(np.array(gp_objects_sort))
    sorted_names = [gp_object_names[i] for i in order]

    return sorted_names
