# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import json

def get_selection_preset_data() :
    json_data = bpy.context.scene.selection_preset_data
    
    if json_data == "" :
        return {}
    
    try :
        data = json.loads(json_data)
        return data
    
    except ValueError :
        return {}

def set_selection_preset_data(data) :
    bpy.context.scene.selection_preset_data = json.dumps(data)


def add_new_preset(name) :
    data = get_selection_preset_data()
    active_object = bpy.context.view_layer.objects
    data[name] = list(set([active_object.active.name] + [o.name for o in bpy.context.selected_objects]))
    set_selection_preset_data(data)


def remove_preset(name) :
    data = get_selection_preset_data()
    del data[name]
    set_selection_preset_data(data)

