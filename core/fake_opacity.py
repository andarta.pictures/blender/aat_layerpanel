# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from collections import Counter

from ..common import gp_utils
from ..common.utils import register_classes, unregister_classes


def update_active_layer_opacity(self, context) :
    gp_object = gp_utils.get_active_gp_object()
    active_layer_id = get_active_layer_index()

    if gp_object is None or active_layer_id is None :
        return
    
    if len(gp_object.data.layers) != len(gp_object.data.layer_fake_opacity_list) :
        return

    update_layer_opacity(gp_object, active_layer_id)


def update_all_layer_opacity(self, context) :
    gp_object = gp_utils.get_active_gp_object()

    if len(gp_object.data.layers) != len(gp_object.data.layer_fake_opacity_list) :
        update_fake_layer_opacity(gp_object)
        
    for i in range(0, len(gp_object.data.layers)) :
        update_layer_opacity(gp_object, i)


def update_layer_opacity(gp_object, layer_id) :
    object_opacity = gp_object.data.fake_opacity
    active_layer = gp_object.data.layers[layer_id]
    layer_fake_opacity = gp_object.data.layer_fake_opacity_list[layer_id].opacity
    active_layer.opacity = object_opacity*layer_fake_opacity


class GPO_VIEW_fake_opacity(bpy.types.PropertyGroup) :
    opacity : bpy.props.FloatProperty(name="Opacity", default=1.0, max=1.0, min=0.0, update=update_active_layer_opacity)
    layer_name : bpy.props.StringProperty(name="Layer Name", default="")

    def set_layer_name(self, layer_name) :
        self.layer_name = layer_name
    
    def set_opacity(self, opacity) :
        self.opacity = opacity


def layer_opacity_changed(gp_obj) :
    gp = gp_obj.data
    fake_names = [e.layer_name for e in gp.layer_fake_opacity_list]
    real_names = [l.info for l in gp.layers]
    return set(fake_names) != set(real_names)
    

def update_fake_layer_opacity(gp_obj) :
    object_opacity = gp_obj.data.fake_opacity

    fake_opacity_list = gp_obj.data.layer_fake_opacity_list
    fake_opacity_names = [e.name for e in fake_opacity_list]
    fake_opacity_values = [e.opacity for e in fake_opacity_list]

    previous_values = {}
    for i in range(0, len(fake_opacity_names)) :
        name = fake_opacity_names[i]
        opacity = fake_opacity_values[i]

        if name not in previous_values.keys() :
            previous_values[name] = opacity
    
    fake_opacity_list.clear()

    for l in gp_obj.data.layers : 
        fake_op = fake_opacity_list.add()
        fake_op.layer_name = l.info
        
        if l.info in previous_values.keys() :
            fake_op.opacity = previous_values[l.info]

        else :
            if object_opacity == 0.0 :
                fake_op.opacity = 1.0
            else :
                fake_op.opacity = min(l.opacity / object_opacity, 1.0)
            

def get_fake_opacity(obj, layer_name) :
    for fo in obj.data.layer_fake_opacity_list :
        if fo.layer_name == layer_name :
            return fo
    return None


def update_fake_opacities() :
    gp_objects = gp_utils.get_all_gp_objects()

    for obj in gp_objects :
        if layer_opacity_changed(obj) :
            update_fake_layer_opacity(obj)


def get_active_layer_index() :
    active_object = gp_utils.get_active_gp_object()
    
    if active_object is None :
        return None
    
    layers = active_object.data.layers
    # active_layer = layers.active

    if layers.active is None :
        return None

    for i in range(0, len(layers)) :
        if layers[i].info == layers.active.info : 
            return i
    
    return None


classes = [GPO_VIEW_fake_opacity]

def register() :
    register_classes(classes)
    bpy.types.GreasePencil.layer_fake_opacity_list = bpy.props.CollectionProperty(type=GPO_VIEW_fake_opacity)
    bpy.types.GreasePencil.current_layer_fake_opacity = GPO_VIEW_fake_opacity

def unregister() :
    del bpy.types.GreasePencil.current_layer_fake_opacity
    del bpy.types.GreasePencil.layer_fake_opacity_list
    unregister_classes(classes)