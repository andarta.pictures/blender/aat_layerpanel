# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

from mathutils import Matrix, Vector
import numpy as np

from ..common import geometry_3D as geo3D, gp_utils

def sync_materials(material_name_list, src_obj, dst_obj):
    """
    Transfers any missing materials from material_name_list to the destination object.

    - material_name_list : list of material names to transfer
    - src_obj : source object
    - dst_obj : destination object
    
    """

    dst_materials = [mat.name for mat in dst_obj.data.materials if mat is not None]

    for mat_name in material_name_list :
        if mat_name not in dst_materials :
            material = src_obj.data.materials.get(mat_name)
            assert material is not None, "Material %s not found in source object %s" % (mat_name, dst_obj.name)
            dst_obj.data.materials.append(material)


def z_push_stroke_sections(stroke_sections, src_obj, dst_obj) :
    """
    Edits point coordinates in each stroke_section to push them to the 
    relevant depth, while keeping visual aspect intact.

    - stroke_section : list of dict object containing original stroke, material 
    name, and point-wise lists of coordinates, pressure and strength
    - src_obj : source object
    - dst_obj : destination object

    Returns the modified stroke_section list.
    
    """
    
    r3d = bpy.context.space_data.region_3d
    camera_location = np.array(r3d.view_matrix.inverted().translation)

    matrix1 = Matrix(src_obj.matrix_world)
    world_mat = np.asarray(matrix1)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]
    
    matrix2 = Matrix(dst_obj.matrix_world)
    world_mat_2 = np.asarray(matrix2)
    mat_2 = world_mat_2[:3, :3]
    loc_2 = world_mat_2[:3, 3]
    inv_mat_2 = np.linalg.inv(mat_2.T)

    dst_origin = np.array(dst_obj.location)
    
    for section in stroke_sections:
        zpushed_coordinates = []
        
        for coord in section["coordinates"] :
            world_coord = np.array(coord) @ mat.T + loc
            world_coord = geo3D.calculate_intersection(dst_origin, np.array([0,1,0]), camera_location, world_coord-camera_location)
            local_coord = (world_coord - loc_2) @ inv_mat_2
            zpushed_coordinates.append(Vector(local_coord.tolist()))
        
        section["coordinates"] = zpushed_coordinates
    
    return stroke_sections

def gather_strokes(layer) :
    """
    Gathers strokes list from a layer, with relevant checks.

    """
    if layer.active_frame is None :
        return []

    strokes = []
    for s in layer.active_frame.strokes :
        strokes.append(s)

    return strokes

def get_selection_sections(selection, points_n) :
    """
    From a list of selected indices, will return a list of continuous ranges. E.g : [1,2,3,8,9] -> [[1,2,3], [8,9]]

    - selection : list of indices, in ascendant order
    - points_n : total number of elements (to loop first and last ranges if necessary)

    """

    if len(selection) == 0 :
        return []

    sections = [[selection[0]]]

    for i in selection[1:] :
        current_section = sections[-1]
        previous_i = current_section[-1]
        if i > previous_i+1 :
            sections.append([i])
        else :
            current_section.append(i)

    if len(sections) > 1 and sections[-1][-1] == points_n-1 :
        sections = [sections[-1]+sections[0]] + sections[1:-1]

    return sections

def extract_stroke_sections(stroke, src_object, dst_object, selected_only = True) :
    """
    Extracts a dict of relevant data from a stroke, corresponding to the 
    ranges selected by the user.

    - stroke : grease pencil stroke
    - src_object : source object

    Returns a list of dicts containing original stroke, material 
    name, and point-wise lists of coordinates, pressure and strength
    
    """

    material_name = src_object.data.materials[stroke.material_index].name

    if selected_only:
        selection = [i for i in range(0,len(stroke.points)) if stroke.points[i].select]
        selection_sections = get_selection_sections(selection, len(stroke.points))
    else :
        selection_sections = [[i for i in range(0,len(stroke.points))]]

    stroke_sections = []

    scale_ratio = dst_object.scale.x / src_object.scale.x

    for section in selection_sections :
        coordinates = [stroke.points[i].co for i in section]
        pressure = [stroke.points[i].pressure / scale_ratio for i in section]
        strength = [stroke.points[i].strength for i in section]
        
        stroke_section = {
            "coordinates" : coordinates,
            "pressure" : pressure,
            "strength" : strength,
            "stroke" : stroke,
            "material_name" : material_name
        }
        
        stroke_sections.append(stroke_section)
    
    return stroke_sections


def material_name_to_index(material_name, gp_obj) :
    """
    Recovers grease pencil object specific index of material from its name.

    - material_name : name of the material
    - gp_obj : grease pencil object to recover the index from

    Returns index of the material.
    
    """

    for i, material in enumerate(gp_obj.data.materials):
        if material is not None and material.name == material_name:
            return i
    
    print("Material '%s' not found." % material_name)

def create_stroke(stroke_section, dst_obj, dst_frame) :
    """
    Convert a stroke section list back to new strokes at the destination frame.

    - stroke_section : list of dict object containing original stroke, material 
    name, and point-wise lists of coordinates, pressure and strength
    - dst_obj : destination object
    - dst_frame : destination frame

    """

    new_stroke = dst_frame.strokes.new()

    coordinates = stroke_section["coordinates"]
    strength = stroke_section["strength"]
    pressure = stroke_section["pressure"]
    material_name = stroke_section["material_name"]
    stroke = stroke_section["stroke"]

    material_index = material_name_to_index(material_name, dst_obj)

    assert material_index is not None, "Material named %s not found in object '%s'"%(material_name, dst_obj.name)

    new_stroke.material_index = material_index
    new_stroke.line_width = stroke.line_width

    new_stroke.points.add(len(coordinates))

    for i in range(0, len(coordinates)) :
        new_stroke.points[i].pressure = pressure[i]
        new_stroke.points[i].strength = strength[i]
        new_stroke.points[i].co = coordinates[i]

def copy_selection_to_layer(src_obj, dst_obj, src_layer, dst_layer, frame_cible = 0) :
    """
    Copies selected points from one layer to another, be it from the same or two different objects.
    
    - src_obj : source object
    - dst_obj : destination object
    - src_layer : source layer
    - dst_layer : destination layer

    """

    scene = bpy.context.scene
    strokes = gather_strokes(src_layer)

    pop = lambda l : l[0] if len(l) > 0 else None
    dst_frame = pop([f for f in dst_layer.frames if f.frame_number == frame_cible])
    if dst_frame is None :
        print("Creating new frame for %s>%s at frame %d" % (dst_obj.name, dst_layer.info, frame_cible))
        dst_frame = dst_layer.frames.new(frame_cible)


    # if dst_layer.active_frame is not None :
    #     dst_frame = dst_layer.active_frame
    #     #try to get frame at frame_cible

    # else :
    #     print("Creating new frame for %s>%s at frame %d" % (dst_obj.name, dst_layer.info, 0))
    #     dst_frame = dst_layer.frames.new(0)

    stroke_sections = []
    for s in strokes :
        stroke_sections += extract_stroke_sections(s, src_obj, dst_obj,
                                                   selected_only = scene.gp_copy_selected_only
                                                   )

    materials_to_sync = list(set([s["material_name"] for s in stroke_sections]))
    sync_materials(materials_to_sync, src_obj, dst_obj)
    
    if scene.gp_copy_autoscale : 
        stroke_sections = z_push_stroke_sections(stroke_sections, src_obj, dst_obj)
    
    for section in stroke_sections :
        create_stroke(section, dst_obj, dst_frame)

    if not scene.gp_copy_keepgeo :
        strokes_to_delete = list(set([s["stroke"] for s in stroke_sections]))
        for stroke in strokes_to_delete :
            src_layer.active_frame.strokes.remove(stroke)

    gp_utils.refresh_geometry_display(dst_obj)

def multiframe_copy_selection_to_layer(src_obj, dst_obj, src_layer, dst_layer) :
    """
    Iterate trough a range of frames and copy the selection from the source layer to the destination layer.
    """

    scene = bpy.context.scene
    #for eahc frame of the src layer
    for frame in reversed(src_layer.frames):

        bpy.context.scene.frame_set(frame.frame_number)
        # src_layer.active_frame = frame
        copy_selection_to_layer(src_obj, dst_obj, src_layer, dst_layer,frame_cible=frame.frame_number)
        dst_layer.active_frame.frame_number = frame.frame_number