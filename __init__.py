# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



bl_info = {
    "name" : "AAT_LayerPanel",
    "author" : "Andarta",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (1, 13),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}


from . import (
    ops,
    ui,
    core,
)


def register():
    print("Registering draw module.")
    ops.register()
    ui.register()
    core.register()


def unregister():
    print("Unregistering draw module.")
    core.unregister()
    ui.unregister()
    ops.unregister()
