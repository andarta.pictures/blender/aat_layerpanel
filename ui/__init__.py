# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.
import bpy
import traceback

from ..common.utils import register_classes, unregister_classes
from ..common import gp_utils, timer

from .. import core
from . import display_elements, hierarchy_analysis, ui_lists, menus


class GP_Layer_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "GP Layers"
    bl_idname = "OBJECT_PT_gplayers"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):

        active_gp = core.get_active_gp_object()

        layout = self.layout

        if context.scene.pause_layer_panel :
            row = layout.row()
            row.label(text="This panel was paused by another module.")
            row = layout.row()
            row.operator("anim.unpause_layer_panel", icon="TRIA_RIGHT", text="Resume")
            return 

        row = layout.row()
        col = row.column()
        
        row2 = col.row(align=True)

        subrow = row2.row(align=True)
        if active_gp is not None :
            subrow.prop(active_gp.data, "fake_opacity", slider=True, emboss=True, text="Object opacity")

        else :
            subrow.prop(context.scene, "dummy_fake_opacity", slider=True, emboss=True, text="Object opacity")
            subrow.enabled = False

        row2.operator("anim.gp_place_gp_origin", icon = "PIVOT_BOUNDBOX", text="")
        row2.operator("anim.layer_preset_menu_open", icon="FILE_CACHE", text="")
        row2.operator("anim.selection_preset_menu_open", icon="STICKY_UVS_LOC", text="")
        row2.operator("anim.gp_obj_test", icon = "MODIFIER", text="")

        col.template_list(
            ui_lists.SCENE_UL_gpencil_objects.bl_idname,
            "",
            context.scene,
            "gp_hierarchy_list",
            context.scene,
            "active_gp_index",
            type="DEFAULT",
            columns=2,
            maxrows=10,
            rows=3,
        )

        col = row.column()
        subcol = col.column(align=True)
        subcol.operator("anim.gp_obj_add", icon = "ADD", text="")
        subcol.operator("anim.gp_obj_remove", icon = "REMOVE", text="")
        col.separator(factor=1)
        col.operator("anim.gp_move_to_collection_menu", icon = "OUTLINER_COLLECTION", text="")

        # List of grease pencil objects in the scene

        if active_gp is not None :

            layout.separator(factor=1.5)

            row = self.layout.row()

            col = row.column()
            
            row2 = col.row(align=True)
            active_layer_index = core.fake_opacity.get_active_layer_index()

            if active_layer_index is not None :
                try :
                    row2.prop(active_gp.data.layer_fake_opacity_list[active_layer_index], "opacity", text="Layer opacity : ", slider=True, emboss=True)
                except IndexError as e : 
                    core.event_manager.force_fake_opacity_update(active_gp)
                    pass

                row2.prop(active_gp.data, "use_autolock_layers", text="", icon="LOCKVIEW_ON" if active_gp.data.use_autolock_layers else "LOCKVIEW_OFF")
                row2.operator("gpencil.layer_merge", icon = "TRIA_DOWN_BAR", text="").mode = "ACTIVE"
                row2.operator("anim.gp_copy_selected_menu", icon = "COPYDOWN", text="")

            col.template_list(
                ui_lists.GPENCIL_UL_draw_layer.bl_idname,
                "",
                active_gp.data,
                "layers",
                active_gp.data.layers,
                "active_index",
                type="DEFAULT",
                columns=2,
                rows=4,
                maxrows=6,
                sort_reverse=True,
                sort_lock=True,
            )

            col = row.column()
            subcol = col.column(align=True)
            subcol.operator("anim.gp_layer_add", icon = "ADD", text="")
            subcol.operator("gpencil.layer_remove", icon = "REMOVE", text="")

            col.separator(factor=1)
            subcol = col.column(align=True)
            subcol.operator("gpencil.layer_move", icon = "TRIA_UP", text="").type = 'UP'
            subcol.operator("gpencil.layer_move", icon = "TRIA_DOWN", text="").type = 'DOWN'

            col.separator(factor=1)
            subcol = col.column(align=True)
            subcol.operator("gpencil.layer_duplicate", icon="DUPLICATE", text="").mode = "ALL"
        
owner = object()

classes = [
    GP_Layer_Panel, 
    ui_lists.SCENE_UL_gpencil_objects,
    ui_lists.GPENCIL_UL_draw_layer,
    menus.CollectionMenu, 
    menus.CopySelectionSubMenu, 
    menus.CopySelectionMenu,
    menus.ExtraToolsMenu,
    menus.LayerPresetMenu,
    menus.SelectionPresetMenu,
    menus.SelectionPresetDeleteSubmenu,
    menus.ExtraOptionsMenu,
]

def register() :
    register_classes(classes)

    display_elements.register()

    bpy.types.GreasePencil.fake_opacity = bpy.props.FloatProperty(name="Opacity", default=1.0, max=1.0, min=0.0, update=core.fake_opacity.update_all_layer_opacity)
    bpy.types.Scene.active_gp_index = bpy.props.IntProperty(name="Active GP Index", update=core.event_manager.on_GPO_VIEW_change)
    bpy.types.Scene.layer_preset = bpy.props.StringProperty(name="Layer Preset", default="DEFAULT")
    bpy.types.Scene.dummy_fake_opacity = bpy.props.FloatProperty(name="Opacity", default=1.0, max=1.0, min=0.0)
    bpy.types.Scene.pause_layer_panel = bpy.props.BoolProperty(default=False)
    bpy.types.Scene.gp_copy_filter = bpy.props.StringProperty(default="", update=lambda arg1,arg2 : bpy.ops.wm.call_menu(name="OBJECT_MT_copy_selection_menu"))


    

def unregister() :
    del bpy.types.Scene.gp_copy_filter
    del bpy.types.Scene.pause_layer_panel
    del bpy.types.Scene.layer_preset
    del bpy.types.Scene.active_gp_index
    del bpy.types.GreasePencil.fake_opacity
    display_elements.unregister()
    unregister_classes(classes)