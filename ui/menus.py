# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

from ..common import gp_utils

from .. import core

class CollectionMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_collection_menu"
    bl_label = "Select Collection"

    def draw(self, context):
        layout = self.layout

        collection_names = ["Scene Collection"]+[c.name for c in bpy.data.collections]+["Create new collection..."]

        for c_name in collection_names :
            layout.operator("anim.gp_move_to_collection", text=c_name).move_to = c_name


class CopySelectionSubMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_copy_selection_submenu"
    bl_label = "Copy selection to..."

    def draw(self, context):
        layout = self.layout

        for l in context.gp_object.data.layers :
            layout.operator("anim.gp_copy_selected", text=l.info, icon="GREASEPENCIL").move_to = "%s/%s"%(context.gp_object.name, l.info)
        
        layout.operator("anim.gp_copy_selected", text="New Layer", icon="PLUS").move_to = "%s/%s"%(context.gp_object.name, "New layer")


class CopySelectionMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_copy_selection_menu"
    bl_label = "Copy selection to..."

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        gp_objects = gp_utils.get_all_gp_objects()
        active_object = core.get_active_gp_object()

        layout.separator()
        row = layout.row()
        row.prop(scene, "gp_copy_autoscale", text="Project from view")
        row = layout.row()
        row.prop(scene, "gp_copy_keepgeo", text="Keep original strokes")
        row = layout.row()
        row.prop(scene, "gp_copy_multiframe", text="Multiframe copy")
        row = layout.row()
        row.prop(scene, "gp_copy_selected_only", text="Selected strokes/points only")
        
        layout.separator()
        layout.prop(scene, "gp_copy_filter", text="", icon="FILTER")
        layout.separator()
        
        for gp in gp_objects :
            if scene.gp_copy_filter.lower() in gp.name.lower() :
                row = layout.row()
                row.context_pointer_set("gp_object", bpy.data.objects[gp.name])
                row.menu(CopySelectionSubMenu.bl_idname, text=gp.name, icon="STROKE")

        layout.separator()
        row = layout.row()
        row.menu("GPENCIL_MT_move_to_layer", text=f"{active_object.name} (local)", icon="GP_SELECT_STROKES")
        
        


class ExtraToolsMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_extra_tools_menu"
    bl_label = "Extra layer tools"

    def draw(self, context):
        layout = self.layout
        scene = bpy.context.scene

        layout.operator("anim.gp_copy_selected_menu", icon = "COPYDOWN", text="Copy selected strokes to...")
        layout.operator("gpencil.layer_merge", icon = "TRIA_DOWN_BAR", text="Merge down layer").mode = "ACTIVE"
        
        gp_object = gp_utils.get_active_gp_object()
        layout.prop(gp_object.data, "use_autolock_layers", text="Autolock inactive layers")
        # anim.gp_place_gp_origin
        layout.operator("anim.gp_place_gp_origin", icon = "PIVOT_BOUNDBOX", text="Place Grease Pencil origin...")
        # bpy.data.cameras["Camera"].show_passepartout
        layout.prop(scene.camera.data, "show_passepartout", text="Camera Passe-Partout", icon="HOLDOUT_ON")


class LayerPresetMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_layer_preset_menu"
    bl_label = "Layer presets"

    def draw(self, context):
        layout = self.layout
        scene = bpy.context.scene

        for preset in ["DEFAULT", "MODELSHEET", "ANIM"] :
            icon = "CHECKBOX_HLT" if scene.layer_preset == preset else "CHECKBOX_DEHLT"
            layout.operator("anim.layer_preset_edit", icon=icon, text=preset).name = preset


class SelectionPresetMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_selection_preset_menu"
    bl_label = "Selection presets"

    def draw(self, context):
        layout = self.layout
        layout.operator_context = "INVOKE_DEFAULT"

        preset_names = list(core.selection_preset.get_selection_preset_data().keys())
        preset_names.sort()
        
        for name in preset_names :
            layout.operator("anim.selection_preset_edit", icon="SEQ_STRIP_META", text=name).name = name
        
        if len(preset_names) > 0 :
            layout.separator()
            layout.menu("OBJECT_MT_selection_preset_delete_submenu", text="Delete preset", icon="TRASH")
        
        layout.operator("anim.selection_preset_create", icon="PLUS", text="New preset...")


class SelectionPresetDeleteSubmenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_selection_preset_delete_submenu"
    bl_label = "Selection presets"

    def draw(self, context):
        layout = self.layout

        preset_names = list(core.selection_preset.get_selection_preset_data().keys())
        preset_names.sort()
        
        for name in preset_names :
            layout.operator("anim.selection_preset_remove", icon="SEQ_STRIP_META", text=name).name = name

class ExtraOptionsMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_extra_options_menu"
    bl_label = "Extra options"

    def draw(self, context):
        layout = self.layout

        name = context.display_element.object_name
        e_type = context.display_element.type

        for n in ["A", "B", "C"] :
            layout.label(text=name+" : "+n)
