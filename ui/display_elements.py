# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from abc import ABC, abstractmethod
import traceback

from ..common import gp_utils, timer, blender_ui, gp_utils
from ..common.utils import register_classes, unregister_classes

from .. import core, ui


indentation_index = {}
folded_collections = []
previously_selected_objects = []
previous_visible_status = {}
previously_active_object = ""


def sync_selection() :
    global previously_selected_objects

    collection = bpy.context.scene.gp_hierarchy_list
    UI_selected_objects = [e.name for e in collection if "OBJECT" in e.type and e.selected]
    state_selected_objects = [o.name for o in bpy.context.selected_objects]
    changed = False

    viewlayer_object_names = [o.name for o in bpy.context.view_layer.objects]

    if set(state_selected_objects) != set(previously_selected_objects) :
        changed = True
        for e in collection :
            e.selected = e.name in state_selected_objects

    if set(UI_selected_objects) != set(previously_selected_objects) :
        changed = True
        for o in bpy.data.objects :
            if o.name in viewlayer_object_names and o.library is None :
                o.select_set(o.name in UI_selected_objects)

    if changed :
        blender_ui.full_redraw()
    
    previously_selected_objects = [o.name for o in bpy.context.selected_objects]

    return changed


def update_active_object() :
    global previously_active_object

    id = bpy.context.scene.active_gp_index
    collection = bpy.context.scene.gp_hierarchy_list

    if id not in range(0, len(collection)) :
        return 

    item = collection[id]

    if "COLLECTION" in item.type :
        return

    obj = bpy.data.objects[item.name]

    if obj is None :
        return
    
    UI_active_name = item.name


    if UI_active_name != previously_active_object :
        viewlayer_object_names = [o.name for o in bpy.context.view_layer.objects]
        if obj.name in viewlayer_object_names :
            bpy.context.view_layer.objects.active = obj
            for o in bpy.context.view_layer.objects :
                if o.library is None and o.name in viewlayer_object_names :
                    o.select_set(o.name == UI_active_name)
    
    state_active_object = bpy.context.view_layer.objects.active

    if state_active_object is not None :
        state_active_name = state_active_object.name
        
        if state_active_name != previously_active_object :
            ids = [i for i in range(0, len(collection)) if collection[i].name == state_active_name]

            if len(ids) == 0 :
                print("Could not find object %s in the display elements." % state_active_name)
            
            bpy.context.scene.active_gp_index = ids[0]
    
    state_active_object = bpy.context.view_layer.objects.active
    previously_active_object = state_active_object.name if state_active_object is not None else "None"


def list_diff(new_list, old_list) :
    added = [e for e in new_list if e not in old_list]
    removed = [e for e in old_list if e not in new_list]
    msg = []

    if len(added) == 0 and len(removed) == 0 :
        return "no change."
    
    if len(added) > 0 :
        msg.append("%s added" % ", ".join(added))
    
    if len(removed) > 0 :
        msg.append("%s removed" % ", ".join(removed))
    
    return " ; ".join(msg)+"."


def sync_visible_status() :
    global previous_visible_status

    scene = bpy.context.scene

    try :

        object_index = {o.name:o for o in scene.objects}
        display_element_index = {e.name:e for e in bpy.context.scene.gp_hierarchy_list}

        common_names = [name for name in object_index if name in display_element_index]

        change_log = []

        for name in common_names :
            e = display_element_index[name]
            o = object_index[name]

            if name not in previous_visible_status : 
                break
            previous_status = previous_visible_status[name]

            # if object visibility was changed, update display element visibility
            object_status = not o.hide_get()
            if object_status != previous_status : 
                e.visible = object_status
                o.hide_render = not object_status
                change_log.append(name)

            # if display element visibility was changed, update object visibility
            display_element_status = e.visible
            if display_element_status != previous_status : 
                o.hide_set(not display_element_status)
                o.hide_render = not display_element_status
                change_log.append(name)

        change_log = set(change_log)
        if len(change_log) > 0 :
            print(f"Synced visibility of {len(change_log)} objects")
            blender_ui.full_redraw()
    

    except RuntimeError as e :
        print("Error during visibilty update.")
        reset_collection()
    
    finally :
        previous_visible_status = {name : not object_index[name].hide_get() for name in common_names}

    return len(change_log) > 0
    

def get_indent(name) :
    if name in indentation_index.keys() :
        return indentation_index[name]
    
def get_folded_collections() :
    global folded_collections
    return folded_collections

def is_folded(name) :
    return name in folded_collections


def set_folded(name, status) :
    global folded_collections

    if status == True and name not in folded_collections :
        folded_collections.append(name)
    
    elif status == False and name in folded_collections :
        folded_collections = [n for n in folded_collections if n != name]


def path_is_folded(path) :
    if len(path) == 0 :
        return False
    
    parent_names = path.split("/")
    parent_names = [p for p in parent_names if len(p) > 0]

    folded_status = []

    for p_name in parent_names :
        folded_status.append(is_folded(p_name))
    
    return True in folded_status


def get_children(name) :
    collection = bpy.context.scene.gp_hierarchy_list
    children = []

    for element in collection :
        parent_names = element.path.split("/")
        parent_names = [p for p in parent_names if len(p) > 0]

        if name in parent_names :
            children.append(element)
    
    return children
    

def reset_collection() :
    collection = bpy.context.scene.gp_hierarchy_list

    for i in reversed(range(0, len(collection))) :
        collection.remove(i)
    
    assert len(collection) == 0, "Display collection could not be emptied."


def update_content() :
    collection = bpy.context.scene.gp_hierarchy_list

    if len(collection) == 0 :
        initialize_content()
    
    update_active_object()
    sync_selection()
    sync_visible_status()


def initialize_content() :
    global indentation_index, previously_selected_objects

    scene = bpy.context.scene
    collection = scene.gp_hierarchy_list

    hierarchy = ui.hierarchy_analysis.get_hierarchy_list()
    indentation_index = {}
    visible_objects = [o.name for o in scene.objects if o.hide_get() == False]

    for i, data in enumerate(hierarchy) :
        item_type = data[0]
        name = data[1]
        indent = data[2]
        path = data[3]

        item = collection.add()
        item.name = name
        item.type = item_type
        item.path = path
        item.h_order = i
        item.selected = name in previously_selected_objects
        item.visible = name in visible_objects

        if "COLLECTION" in item_type :
            active = data[5]
            if not active :
                item.type = "INACTIVE_COLLECTION"

        indentation_index[name] = indent


class GPO_VIEW_display_element(bpy.types.PropertyGroup) :
    name : bpy.props.StringProperty(name="Name", default="Unknown")
    type : bpy.props.StringProperty(name="Type", default="Unknown")
    path : bpy.props.StringProperty(name="Path", default="Unknown")
    h_order : bpy.props.IntProperty(name="Hierarchical order", default=0)
    selected : bpy.props.BoolProperty(name="Selected", default=False)
    visible : bpy.props.BoolProperty(name="Visible", default=True)


classes = [
    GPO_VIEW_display_element,
]

def register() :
    register_classes(classes)
    bpy.types.Scene.gp_hierarchy_list = bpy.props.CollectionProperty(type=GPO_VIEW_display_element)

def unregister() :
    unregister_classes(classes)
    del bpy.types.Scene.gp_hierarchy_list