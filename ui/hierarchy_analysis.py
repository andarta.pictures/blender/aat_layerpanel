# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

from ..common.utils import register_classes, unregister_classes
from ..common import gp_utils, timer

from ..common.gp_utils import get_layer_collection


def recursive_collection_seek(element, depth=0) :
    if depth > 10 :
        print("Max depth reached.")
        return None

    if isinstance(element, bpy.types.Collection) :
        subhierarchy = []
        
        children = [element.children[k] for k in element.children.keys()] + [element.objects[k] for k in element.objects.keys()]

        for child in children :
            out = recursive_collection_seek(child, depth=depth+1)

            if out is not None :
                subhierarchy.append(out)
        
        return element.name, subhierarchy

    elif isinstance(element, bpy.types.Object) :
        if isinstance(element.data, bpy.types.GreasePencil) :
            return "GP_OBJECT:"+element.name
        elif isinstance(element.data, bpy.types.Mesh) :
            return "MESH_OBJECT:"+element.name
        elif isinstance(element.data, bpy.types.Camera) :
            return "CAM_OBJECT:"+element.name
        elif isinstance(element.data, bpy.types.Image) :
            return "IMREF_OBJECT:"+element.name
        else : 
            return "UNKNOWN_OBJECT:"+element.name


def unpack_hierarchy(hierarchy, current_list=None, depth=0, current_path="") :
    if depth > 10 :
        print("Max depth reached.")
        return None
    
    if current_list is None :
        current_list = list()

    for e in hierarchy :
        if isinstance(e, tuple) :
            content = [el[1] for el in unpack_hierarchy(e[1], [], depth+1, "")]
            content = ", ".join(content)
            current_list.append(("COLLECTION", e[0], depth, current_path, content))
            current_list = unpack_hierarchy(e[1], current_list, depth+1, current_path+e[0]+"/")
        elif isinstance(e, str) :
            e_type, name = e.split(":")
            current_list.append((e_type, name, depth, current_path, ''))
    
    return current_list


def get_hierarchy_list() :
    hierarchy = recursive_collection_seek(bpy.context.scene.collection)
    unpacked_hierarchy = unpack_hierarchy(hierarchy[1])
    lc_index = get_layer_collection_index()

    for i,e in enumerate(unpacked_hierarchy) :
        if e[0] == "COLLECTION" :
            layer_collection = lc_index[e[1]]
            unpacked_hierarchy[i] = tuple(list(e) + [layer_collection.visible_get()])

    return unpacked_hierarchy


def get_collection(name) :
    for collection in bpy.data.collections:
        if collection.name == name :
            return collection


def get_layer_collection_index() :
    view_layer = bpy.context.view_layer

    def collect_children_collections(col, result=None):
        collections = [col]

        for c in col.children:
            collections += collect_children_collections(c)
        
        return collections

    layer_collections = collect_children_collections(view_layer.layer_collection)

    col_index = {}

    for lc in layer_collections :
        col_index[lc.name] = lc

    return col_index