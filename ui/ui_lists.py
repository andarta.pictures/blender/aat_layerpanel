# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from time import time

from ..common.utils import register_classes, unregister_classes
from ..common import timer

from .. import core, ui

helper_funcs = bpy.types.UI_UL_list
base_indent = 0

filter_cache = None
last_statehash = None

def get_indent(path) :
    if len(path) == 0 :
        return 0
    
    return len([c for c in path if c == "/"])+1

class SCENE_UL_gpencil_objects(bpy.types.UIList):
    """
    Template to display the grease pencil objects of a scene.
    """

    bl_idname = "SCENE_UL_gpencil"
    indent_size = 1.2

    def __init__(self) :
        super().__init__()
        core.gp_tools.set_gp_object_filter_list(self)
        self.elements_displayed = None

    def _gen_order_update(name1, name2):
        """Code snippet to detoggle one button when the other is pressed."""
        def _u(self, ctxt):
            if (getattr(self, name1)):
                setattr(self, name2, False)
            core.event_manager.force_state_update()
                
        return _u
    
    use_order_hierarchy: bpy.props.BoolProperty(
        name="Hierarchy", 
        default=False, 
        options=set(),
        description="Sort Grease Pencil objects by their position in Blender hierarchy",
        update=_gen_order_update("use_order_hierarchy", "use_order_depth"),
    )
    
    use_order_depth: bpy.props.BoolProperty(
        name="Depth",
        default=True,
        options=set(),
        description="Sort Grease Pencil objects by their depth",
        update=_gen_order_update("use_order_depth", "use_order_hierarchy"),
    )
    
    display_all_collections : bpy.props.BoolProperty(
        name="Display all collections",
        default=True,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )
    
    display_meshes : bpy.props.BoolProperty(
        name="Display meshes",
        default=False,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )
    
    display_refs : bpy.props.BoolProperty(
        name="Display image references",
        default=False,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )
    
    display_cams : bpy.props.BoolProperty(
        name="Display cameras",
        default=False,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )
    
    display_selectable : bpy.props.BoolProperty(
        name="Display selectable property",
        default=False,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )
    
    display_extra_options : bpy.props.BoolProperty(
        name="Display extra options",
        default=False,
        update=lambda self, ctxt : core.event_manager.force_state_update(),
    )

    def get_base_indent(self, elements_displayed) :

        if elements_displayed is None :
            return 0
        
        indents = [ui.display_elements.get_indent(e.name) for e in elements_displayed]
        indents = [indent for indent in indents if indent is not None]
        return min(indents) if len(indents) > 0 else 0


    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        """Draw function of each item, with different behavior for GP_Objects and Collections.
        
        - layout : UI class passed by blender, allows to edit interface.
        - item : GPO_VIEW_display_element to be drawn."""

        global base_indent

        row = layout.row(align=True)

        if self.use_order_hierarchy :
            indent = ui.display_elements.get_indent(item.name) - base_indent
            indent = max(indent, 0)
            
            if indent is None :
                indent = 0
            
            for i in range(0, indent) :
                row.separator(factor=SCENE_UL_gpencil_objects.indent_size)

        icons = {
            "GP_OBJECT" : "GREASEPENCIL",
            "MESH_OBJECT" : "MESH_CUBE",
            "CAM_OBJECT" : "CAMERA_DATA",
            "IMREF_OBJECT" : "FILE_IMAGE",
        }

        if "OBJECT" in item.type :

            if item.name not in bpy.data.objects.keys() :
                return

            obj = bpy.data.objects[item.name]

            if obj is None :
                return

            ob_icon = icons[item.type] if item.type in icons else "NONE"

            subrow = row.row(align=True)
            selectable = not obj.hide_select and not obj.hide_get()
            subrow.prop(item, "selected", icon=ob_icon, text="", emboss=selectable)
            subrow.enabled = selectable

            row.prop(obj, "name", icon="NONE", text="", emboss=False)
            if self.display_selectable :
                row.prop(obj, "hide_select", icon="RESTRICT_SELECT_OFF", text="", emboss=False, toggle=0)

            visibility_icon = "HIDE_ON" if obj.hide_get() else "HIDE_OFF"
            # row.operator("anim.toggle_object_visibility", icon=visibility_icon, text="", emboss=False).name = obj.name
            row.prop(item, "visible", icon=visibility_icon, text="", emboss=False, toggle=0)

        if "COLLECTION" in item.type :

            col = ui.hierarchy_analysis.get_collection(item.name)

            if col is None :
                return
            
            subrow = row.row(align=True)
            fold_icon = "TRIA_RIGHT" if ui.display_elements.is_folded(item.name) else "TRIA_DOWN"
            subrow.operator("anim.fold_collection", icon=fold_icon, text="", emboss=False).name = item.name

            row.prop(col, "name", icon="NONE", text="", emboss=False)

            if self.display_selectable :
                row.prop(col, "hide_select", icon="RESTRICT_SELECT_OFF", text="", emboss=False, toggle=0)
            
            
            if item.type == "COLLECTION" :
                icon = "COLLECTION_"+col.color_tag if col.color_tag != "NONE" else "OUTLINER_COLLECTION"
            else :
                icon = "GROUP"

            row.operator("anim.activate_collection", icon=icon, text="", emboss=False).name = col.name
    
    
    def get_parent_collection(self, element, display_elements) :
        """Returns GPO_VIEW_display_element corresponding to the collection directly parenting the display element.
        
        - element : GPO_VIEW_display_element whose parent is to be found
        - display_elements : list of all GPO_VIEW_display_element"""

        for e in display_elements :
            if e.name == element.path :
                return e
            
    
    def get_children_list(self, element, white_list) :
        children = []

        if len(element.content) == 0 :
            return []

        for s in element.content.split(",") :
            if ":" in s :
                split_content = s.split(":")
                e_name = split_content[0]
                e_type = split_content[1]
                if e_type in white_list :
                    children.append((e_name, e_type))
        
        return children
    

    def is_type_displayed(self, type) :

        if type == "GP_OBJECT" :
            return True

        elif type == "MESH_OBJECT" :
            return self.display_meshes

        elif type == "CAM_OBJECT" :
            return self.display_cams
            

    def filter_item_flag(self, element, display_elements):
        """Filter function defining whether an item should be displayed in the list.
        
        - element : GPO_VIEW_display_element
        
        Returns visibility status of element (0 : hidden, self.bitflag_filter_item : shown)"""
        
        is_displayed = {
            "GP_OBJECT" : True,
            "MESH_OBJECT" : self.display_meshes,
            "CAM_OBJECT" : self.display_cams,
            "IMREF_OBJECT" : self.display_refs,
            "UNKNOWN_OBJECT" : False,
        }

        if "OBJECT" in element.type :
            assert element.type in is_displayed.keys(), "No display information for %s element type."%element.type

        query = self.filter_name.strip().lower()
        
        if len(query) > 0 :
            query_match = query in element.name.lower() or query in element.path.lower()
            if not query_match :
                return 0
        
        if "OBJECT" in element.type :
            if element.name not in self.objects_in_viewlayer :
                return 0

            elif self.use_order_hierarchy and ui.display_elements.path_is_folded(element.path) :
                return 0

            return self.bitflag_filter_item if is_displayed[element.type] else 0
        
        if "COLLECTION" in element.type : 

            if self.use_order_depth : 
                return 0

            elif self.use_order_hierarchy :

                if ui.display_elements.path_is_folded(element.path) :
                    return 0
                
                if self.display_all_collections :
                    return self.bitflag_filter_item
            
                if element.type == "COLLECTION" :
                    children = ui.display_elements.get_children(element.name)
                    children = [c for c in children if self.is_type_displayed(c.type)]
                    return self.bitflag_filter_item if len(children) > 0 else 0
                
        return 0
    
    
    def get_depth_order(self, display_elements) :
        """Returns ordered list of GPO_VIEW_display_element by the depth of their 
        associated object (depth is 0 for collection, which are hidden in depth 
        mode anyways)"""

        global helper_funcs

        # Create a list of (i,depth) with i being current sort id of object
        # and depth being calculated mean depth of the gp_object

        _sort = []

        for i, e in enumerate(display_elements) :

            if e.type == "COLLECTION" or e.type == "INACTIVE_COLLECTION" :
                _sort.append((i, 0.0))

            elif e.name not in bpy.data.objects.keys() :
                _sort.append((i, 0.0))

            else :
                try :
                    obj_name = e.name
                    obj = bpy.data.objects[obj_name]
                    _sort.append((i, obj.location.y))
                except :
                    print("Error while getting depth for : ", e.name, e.type)
                    # print("Display elements : ", ", ".join(["%s : %s"%(el.name, el.type) for el in display_elements]))
                    raise

        # New order based on second element (e[1]=depth) in _sort list
        flt_neworder = helper_funcs.sort_items_helper(_sort, lambda e: e[1], False)

        return flt_neworder
    

    def get_name_order(self, display_elements) :
        """Returns ordered list of GPO_VIEW_display_element by their position 
        in the outliner hierarchy."""

        global helper_funcs

        _sort = []
        for i, e in enumerate(display_elements) :
            order = e.h_order
            _sort.append((i, order))

        # New order based on second element (e[1]=full_name) in _sort list
        flt_neworder = helper_funcs.sort_items_helper(_sort, lambda e: e[1], False)

        return flt_neworder



    def filter_items(self, context: bpy.types.Context, obj, propname):
        """Main function coordinating filtering and ordering of elements in the list."""
        global base_indent, filter_cache, last_statehash
        display_elements = getattr(obj, propname)

        state_changed, last_statehash = core.event_manager.state_changed(last_statehash)

        if not state_changed :
            return filter_cache['filter'], filter_cache['order']

        self.objects_in_viewlayer = [o.name for o in context.view_layer.objects]

        flt_flags = [self.filter_item_flag(e, display_elements) for e in display_elements]

        flt_neworder = []
        if self.use_order_depth :
            flt_neworder = self.get_depth_order(display_elements)
        elif self.use_order_hierarchy :
            flt_neworder = self.get_name_order(display_elements)

        elements_displayed = [display_elements[i] for i in range(0, len(display_elements)) if flt_flags[i] != 0]
        base_indent = self.get_base_indent(elements_displayed)
        
        filter_cache = {
            'last_update' : time(),
            'filter' : flt_flags,
            'order' : flt_neworder
        }

        return flt_flags, flt_neworder


    def draw_filter(self, context, layout):
        """Draw function for the lower part of the area, 
        with the filter bar and the mode buttons."""
        
        
        row = layout.row()

        row.label(text="Display options :")
        
        subrow = row.row(align=True)
        subrow.prop(self, "display_all_collections", icon="GROUP", text="", emboss=True)
        subrow.prop(self, "display_meshes", icon="MESH_CUBE", text="", emboss=True)
        subrow.prop(self, "display_cams", icon="CAMERA_DATA", text="", emboss=True)
        subrow.prop(self, "display_refs", icon="FILE_IMAGE", text="", emboss=True)
        
        subrow = row.row(align=True)
        subrow.prop(self, "display_selectable", icon="RESTRICT_SELECT_OFF", text="", emboss=True)
        # subrow.prop(self, "display_extra_options", icon="THREE_DOTS", text="", emboss=True)
        
        row = layout.row(align=True)

        row.label(text="Order :")
        row.prop(self, "use_order_depth", icon="NODE_COMPOSITING", toggle=True)
        row.prop(self, "use_order_hierarchy", icon="OUTLINER", toggle=True)

        row = layout.row()

        subrow = row.row(align=True)
        subrow.prop(self, "filter_name", text="")




# ================================================




class GPENCIL_UL_draw_layer(bpy.types.UIList):
    """
    Template to display the layers of a grease pencil object.
    """

    bl_idname = "GPENCIL_UL_draw_layer"

    def draw_item(
        self, context, layout, data, item, icon, active_data, active_propname
    ):

        # Layer name
        layout.prop(item, "info", text="", emboss=False)

        # Layer properties
        sub = layout.row(align=True)
        # - Onion skinning
        onion_icon = "ONIONSKIN_ON" if item.use_onion_skinning else "ONIONSKIN_OFF"
        sub.prop(item, "use_onion_skinning", text="", icon=onion_icon, emboss=False)
        # - Visility / Lock
        sub.prop(item, "hide", text="", emboss=False)
        sub.prop(item, "lock", text="", emboss=False)